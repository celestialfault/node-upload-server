const express = require("express")
const fs = require("fs-extra")
const path = require("path")
const upload = require("express-fileupload")
const generate = require("randomstring").generate
const generateSlug = require("random-word-slugs").generateSlug

const app = express()
app.disable("x-powered-by")
// this router is used as a quick workaround for express not having
// any clear support for base urls
const router = express.Router()
const index = fs.readFileSync("static/index.html", "utf-8")

const PORT = process.env.PORT || 8000
// REDIRECT_BASE is kept for backwards compatibility with older setups
const HTTP_BASE = process.env.HTTP_BASE || process.env.REDIRECT_BASE || "/"
const UPLOAD_DIR = process.env.UPLOAD_DIR || path.join(__dirname, "files")
// default max size of ~20 mb
const MAX_SIZE = process.env.MAX_SIZE ? parseInt(process.env.MAX_SIZE) : 20 * 1024 * 1024

function async_wrap(route) {
  return (req, res, next) => {route(req, res, next).catch(next)}
}

const upload_middleware = upload({
  safeFileNames: true,
  preserveExtension: 10,
  abortOnLimit: true,
  limits: {fileSize: MAX_SIZE},
})

function random_name(ext) {
  if(process.env.FRIENDLY_NAMES) {
    return generateSlug(3, {format: "title", categories: {noun: ["media", "technology", "science", "animals", "profession"]}}).split(' ').join('') + ext
  }
  return generate({length: 8}) + ext
}

router.get("/", (req, res) => {
  res.send(index)
})

router.post("/",
  upload_middleware,
  async_wrap(async (req, res) => {
    if(!req.files || !req.files.file) return res.sendStatus(400)

    let ext
    if(req.files.file.name && req.files.file.name.split(".").filter(x => x.length >= 1).length > 1) {
      ext = path.extname(req.files.file.name)
    } else {
      ext = ""
    }

    let name = random_name(ext)
    while(await fs.exists(path.join(UPLOAD_DIR, name))) {
      name = random_name(ext)
    }

    await req.files.file.mv(path.join(UPLOAD_DIR, name))
    console.log(`Created ${name} from POST request`)

    if(req.query.redirect) res.redirect(303, `${HTTP_BASE}${name}`)
    else {
      res.header("Content-Type", "text/plain")
      res.send(`${HTTP_BASE}${name}`)
    }
  })
)

router.put("/:filename",
  upload_middleware,
  async_wrap(async (req, res) => {
    if(!req.files || !req.files.file) return res.sendStatus(400)

    let name = req.params.filename
    let p = path.join(UPLOAD_DIR, name)
    if(await fs.exists(p)) {
      res.sendStatus(409)
      return
    }

    await req.files.file.mv(p)
    console.log(`Created ${name} from PUT request`)

    res.header("Content-Type", "text/plain")

    if(req.query.redirect) res.redirect(303, `${HTTP_BASE}${name}`)
    else {
      res.header("Content-Type", "text/plain")
      res.send(`${HTTP_BASE}${name}`)
    }
  })
)

router.delete("/:filename", async_wrap(async (req, res) => {
  let p = path.join(UPLOAD_DIR, req.params.filename)
  if(await fs.exists(p)) {
    await fs.remove(p)
    res.sendStatus(204)
  } else {
    res.sendStatus(404)
  }
}))

if(process.env.BASE_DIR) {
  app.use(process.env.BASE_DIR, router)
} else {
  app.use(router)
}

app.use((err, req, res, next) => {
  console.error(`Request to ${req.url} failed with method ${req.method}`)
  console.error(err)
  res.sendStatus(500)
})

app.listen(PORT, process.env.LISTEN_HOST || "127.0.0.1", () => {
  console.log(`Listening on port ${PORT}`)
  console.log(`Saving uploaded files to ${UPLOAD_DIR} with a max file size of ${MAX_SIZE} bytes`)
})
